import React, { Component } from 'react';
import './Navbar.css';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { logoutUser } from '../store/actions/AuthActions'

class Navbar extends Component {
    onLogoutClick = e => {
      e.preventDefault();
      this.props.logoutUser();
    };
    render() {
      const { isLoginSuccess } = this.props;
      return (
        <div className="topnav">
          {
            !isLoginSuccess ? (
              <React.Fragment>
                <Link to="/login" >Login</Link>
                <Link to="/register" >Register</Link>
              </React.Fragment>
            ) : (
              <React.Fragment>
                <Link to="/" >Dashboard</Link>
                <button
                  onClick={this.onLogoutClick}
                  style={{
                    width: '150px',
                    borderRadius: '3px',
                    letterSpacing: '1.5px',
                    marginTop: '1rem',
                    float: 'right'
                  }}
                >Logout</button>
              </React.Fragment>
            )
          }

        </div>
      )
    }
}

Navbar.propTypes = {
  isLoginSuccess: PropTypes.bool.isRequired
};

const mapStateToProps = (state) => ({
  isLoginSuccess: state.AuthReducer.isLoginSuccess
});
const mapDispatchToProps = (dispatch) => {
  return {
    logoutUser: (email, password) => dispatch(logoutUser(email, password)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Navbar);

Navbar.propTypes = {
  logoutUser: PropTypes.func
}