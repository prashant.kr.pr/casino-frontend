import React, { Component } from 'react';
import loader from './loader.gif';

export default class Loader extends Component {
  render() {
    return (
      <div>
        <img
          alt="Loading.."
          src={loader}
        />
      </div>
    )
  }
}
