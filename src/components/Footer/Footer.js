import React, { Component } from 'react';
import fbImg from './img/fb.png'
import twImg from './img/tw.png';
import dlImg from './img/dl.png';
import bhImg from './img/bh.png';
import './Footer.css'
import { Link } from 'react-router-dom';

export default class Footer extends Component {
  render() {
    return (
      <div className="container">
        <div
          className="row"
          style={{marginTop: 60}}
        >
          <div className="col-md-12">
            <ul className="footer-menu">
              <center>
                <li><a href="#">HOME</a></li>
                <li><a href="#">ABOUT US</a></li>
                <li><a href="#">GALLERY</a></li>
                <li><a href="#">RESERVATIONS</a></li>
              </center>
            </ul>
            <ul className="footer-social">
              <center>
                <Link
                  className="footer-social"
                  to="#"
                >
                  <img
                    height="20px;"
                    src={fbImg}
                  /> 
                </Link>
                <Link
                  className="footer-social"
                  to="#"
                >
                  <img
                    height="20px;"
                    src={twImg}
                  />
                </Link>
                <Link
                  className="footer-social"
                  to="#"
                >
                  <img
                    height="20px;"
                    src={dlImg}
                  />
                </Link>
                <Link
                  className="footer-social"
                  to="#"
                >
                  <img
                    height="20px;"
                    src={bhImg}
                  />
                </Link>
              </center>
            </ul>
            <p className="footer-text">Copyright ©2020 All rights reserved.
              <a href="#">by Colorlib</a>.
            </p>
          </div>
        </div>
      </div>
    )
  }
}
