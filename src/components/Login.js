import React, { Component } from 'react';
import './Login.css';
import login from '../store/actions/AuthActions';
import { connect } from 'react-redux';
import PropTypes from 'prop-types'

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      uname: '',
      password: '',
    };
  }
  onchange = (e) => {
    e.preventDefault();
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };
  onsubmit = (e) => {
    e.preventDefault();
    const { uname, password } = this.state;
    this.props.login(uname, password);
    this.setState({
      uname: '',
      password: '',
    });
  };
  componentDidMount = () => {
    if(this.props.isLoginSuccess) {
      this.props.history.push('/')
    }
  }
  componentDidUpdate = () => {
    if(this.props.isLoginSuccess) {
      this.props.history.push('/')
    } 
  }
  render() {
    const { isLoginPending, isLoginSuccess, loginError } = this.props;
    return (
      <div className="main-login-div">
        <form
          action=""
          method="POST"
          onSubmit={this.onsubmit}
        >
          <div className="container">
            <label htmlFor="uname">
              <b>Username</b>
            </label>
            <input
              name="uname"
              onChange={(ele) => this.setState({ uname: ele.target.value })}
              placeholder="Enter Username"
              required
              type="text"
              value={this.state.uname}
            />

            <label htmlFor="psw">
              <b>Password</b>
            </label>
            <input
              name="psw"
              onChange={(ele) => this.setState({ password: ele.target.value })}
              placeholder="Enter Password"
              required
              type="password"
              value={this.state.password}
            />
            <button type="submit">Login</button>
          </div>
        </form>
        <div>
          {isLoginPending && <div>Please Wait...</div>}
          {isLoginSuccess && <div>Login Success</div>}
          {loginError && <div>{loginError.error}</div>}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoginPending: state.AuthReducer.isLoginPending,
    isLoginSuccess: state.AuthReducer.isLoginSuccess,
    loginError: state.AuthReducer.loginError,
    userName: state.AuthReducer.userName
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    login: (email, password) => dispatch(login(email, password)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);

Login.propTypes = {
  history: PropTypes.object,
  isLoginPending: PropTypes.bool,
  isLoginSuccess: PropTypes.bool,
  login: PropTypes.func,
  loginError: PropTypes.object,
}