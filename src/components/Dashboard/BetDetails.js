import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import './BetDetails.css';

export default class BetDetails extends Component {
  render() {
    return (
      <div className="col-md-3">
        <div className="right-content">
          <li
            className="dropdown"
            style={{ listStyle: 'none' }}
          >
            <Link
              className="dropdown-toggle"
              data-toggle="dropdown"
              to="#"
            >
                Available Credit:0.00
              <span className="caret" />
            </Link>
            <ul className="dropdown-menu">
              <li><a href="#"><b className="link">profile</b></a></li>
              <li><a href="#"><b className="link">Password</b></a></li>
            </ul>
          </li>

          <div className="switch button">
            <label className="switch">
              <input type="checkbox" />
              <span className="slider round" />
            </label>
          </div>
          <div className="right-detail">
            <p>Betslip</p>
            <hr />
            <ul style={{ listStyle: 'none', display: 'flex', marginLeft: '-20' }}>
              <li style={{ background: 'white', padding: 10, fontSize: 15 }}>Betslip</li>
              <li style={{ background: 'lightgrey', padding: 10, fontSize: 15 }}><a href="#">Open Bets</a></li>
              <li><button type="submit">Edit Stroke</button></li>
            </ul>
            <div className="right-text">Click on the odds to add selections to the betslip.
              <div className="right-detail-box">
                <p>10 over runs MI(CSK vs MI)adv @ 100</p>
                <ul style={{ display: 'flex', listStyle: 'none' }}>
                  <li>runs<br /><span className="point">100</span></li>
                  <li>stake<br />
                    <button
                      className="btn-point"
                      type="submit"
                    >
                    point
                    </button>
                  </li>
                  <li>profit<br /><span className="point">100</span></li>
                </ul>
                <div className="right-btn">
                  <button type="submit">5000</button>
                  <button type="submit">5000</button>
                  <button type="submit">5000</button>
                  <button type="submit">5000</button>
                  <button type="submit">5000</button>
                  <button type="submit">5000</button>
                  <button type="submit">5000</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
