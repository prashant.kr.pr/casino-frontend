import React, { Component } from 'react';
// import { connect } from 'react-redux';
// import { listOfAllUsers } from '../../store/actions/UserActions';
import './Dashboard.css';
// import PropTypes, { array } from 'prop-types';
import MainHeader from '../Header/MainHeader';
import LeftMenubar from './LeftMenubar';
import BetListing from './BetListing';
import BetDetails from './BetDetails';

class Dashboard extends Component {
  // componentDidMount() {
  //   this.props.listOfAllUsers();
  // }
  render() {
    return (
      <React.Fragment>
        <MainHeader />
        <div className="container-fluid">
          <div className="row">
            <div className="header-bg">
              <LeftMenubar />
              <BetListing />
              <BetDetails />
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

// const mapStateToProps = (state) => {
//   return {
//     isListOfAllUsersStatus: state.userReducer.isListOfAllUsersStatus,
//     listOfAllUsersData: state.userReducer.listOfAllUsers,
//     listOfAllUsersError: state.userReducer.listOfAllUsersError
//   };
// };

// const mapDispatchToProps = (dispatch) => {
//   return {
//     listOfAllUsers: () => dispatch(listOfAllUsers()),
//   };
// };
export default Dashboard;
// export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);

// Dashboard.propTypes = {
//   isListOfAllUsersStatus: PropTypes.bool,
//   listOfAllUsers: PropTypes.func,
//   listOfAllUsersData: PropTypes.shape({
//     data: array
//   })
// }