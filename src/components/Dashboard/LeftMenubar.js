import React, { Component } from 'react';
import './LeftMenubar.css';

export default class LeftMenubar extends Component {
  render() {
    return (
      <div className="col-md-2">
        <div className="left-menu">
          <h3>Dashboard</h3>
          <ul>
            <li><i className="favourites-icon icon-star-empty" />Favourites</li>
            <li><i className="fa fa-search" />Cricket</li>
            <li><i className="fa fa-star" />Football</li>
          </ul>
        </div>
      </div>
    )
  }
}
