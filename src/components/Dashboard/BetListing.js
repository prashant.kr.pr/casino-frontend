import React, { Component } from 'react';
import './BetListing.css';
import { BetListingArr } from '../../demoData';

export default class BetListing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      BetListingArr: []
    }
  }
  componentDidMount() {
    if (BetListing && BetListing.length > 0) {
      this.setState({ BetListingArr: BetListingArr })
    }
  }
  render() {
    return (
      <div className="col-md-7">
        <div className="row" >
          <div className="col-md-6">
            <div
              className="left-box"
              style={{ marginLeft: 0 }}
            >
              <h2>Cricket</h2>
            </div>
          </div>
          <div className="col-md-6">
            <div
              className="right-box"
              style={{ float: 'right' }}
            >
              <h2>Score</h2>
            </div>
          </div>
        </div>
        <div className="row">
          {
            this.state.BetListingArr &&
            this.state.BetListingArr.length > 0 &&
            (
              <table>
                <tbody>
                  <tr>
                    <th>Company</th>
                    <th colSpan="2">Contact</th>
                    <th colSpan="2">Country</th>
                  </tr>
                  {
                    this.state.BetListingArr.map((val, index) => (
                      <tr key={index}>
                        <td>{val.company}</td>
                        <td>{val.contact}</td>
                        <td className="pink">{val.pink}</td>
                        <td className="blue">{val.blue}</td>
                        <td>{val.country}</td>
                      </tr>
                    ))
                  }
                </tbody>
              </table>
            )
          }
        </div>
      </div>
    )
  }
}
