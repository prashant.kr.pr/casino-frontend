import React, { Component } from 'react';
import './SubHeader.css';
import {subHeaderItems} from '../../demoData';

class SubHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menuItems: []
    }
  }
  componentDidMount() {
    if(subHeaderItems && subHeaderItems.length > 0) {
      this.setState({menuItems:subHeaderItems})
    }
  }
  render() {
    return (
      <div className="row">
        <div className="col-md-2" />
        <div className="col-md-8">
          <div className="bottom-header">
            <ul>
              {
                this.state.menuItems &&
                this.state.menuItems.length > 0 &&
                this.state.menuItems.map((val, i) => (
                  <li key={i}>{val}</li>
                )) 
              }
            </ul>
          </div>
        </div>
        <div className="col-md-2" />
      </div>
    );
  }
}

export default SubHeader;
