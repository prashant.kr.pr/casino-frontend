import React, { Component } from 'react';
import './Search.css';

class Search extends Component {
  render() {
    return (
      <div className="search-container">
        <form>
          <input
            name="search"
            placeholder="Search.."
            type="text"
          />
          <button
            className="search-btn"
            type="submit"
          >
            <i className="fa fa-search" />
          </button>
        </form>
      </div>
    );
  }
}

export default Search;
