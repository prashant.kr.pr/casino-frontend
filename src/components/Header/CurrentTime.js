import React, { Component } from 'react'
import moment from 'moment';
import './MainHeader.css';

export default class CurrentTime extends Component {
  constructor(props) {
    super(props)
    this.state = {
      currentTime: moment().format('LTS'),
      currentDate: moment().format('l')
    }
  }
  componentDidMount() {
    setInterval(() => {
      this.setState({
        currentTime: moment().format('LTS'),
        currentDate: moment().format('l')
      })
    }, 1000)
  }
  render() {
    return (
      <div className="time">
        <ul>
          <li>Date: {this.state.currentDate}</li>
          <li>Time: {this.state.currentTime}</li>
        </ul>
      </div>
    )
  }
}
