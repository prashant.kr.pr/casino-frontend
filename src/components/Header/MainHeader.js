import React, { Component } from 'react';
import Search from './Search';
import './MainHeader.css';
import SubHeader from './SubHeader';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { logoutUser } from '../../store/actions/AuthActions';
import CurrentTime from './CurrentTime';

class MainHeader extends Component {
  
  onLogoutClick = e => {
    e.preventDefault();
    this.props.logoutUser();
  };
  render() {
    return (
      <nav
        className="navbar"
        style={{ marginBottom: 0 }}
      >
        <div className="container-fluid">
          <div className="row">
            <div className="top-header">
              <div className="col-md-2">
                <h1>PROJECT</h1>
              </div>
              <div className="col-md-2">
                <CurrentTime />
              </div>
              <div className="col-md-4">
                <Search />
              </div>
              <div className="col-md-2">
                <div className="user-detail">
                  <p>logged in as USER</p>
                  <p>Last logged at:oct. 22 2020</p>
                </div>
              </div>
              <div className="col-md-2">
                <ul
                  style={{ display: 'flex', listStyle: 'none', marginTop: 22 }}
                >
                  <li className="dropdown">
                    <a
                      className="dropdown-toggle"
                      data-toggle="dropdown"
                      href="#"
                    >
                      Account
                      <span className="caret" />
                    </a>
                    <ul className="dropdown-menu">
                      <li>
                        <a href="#">
                          <b className="link">profile</b>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <b className="link">Password</b>
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li style={{ marginLeft: 30 }}>
                    <a onClick={this.onLogoutClick}>Logout</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <SubHeader />
        </div>
      </nav>
    );
  }
}

MainHeader.propTypes = {
  isLoginSuccess: PropTypes.bool.isRequired,
  logoutUser: PropTypes.func,
};

const mapStateToProps = (state) => ({
  isLoginSuccess: state.AuthReducer.isLoginSuccess,
});
const mapDispatchToProps = (dispatch) => {
  return {
    logoutUser: (email, password) => dispatch(logoutUser(email, password)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MainHeader);
