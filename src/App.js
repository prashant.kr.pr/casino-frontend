import React from 'react';
import './App.css';
import Login from './components/Login';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Register from './components/Register';
import Dashboard from './components/Dashboard/Dashboard';
import PrivateRoute from './components/PrivateRoute';
import store from './configureStore';
import jwt_decode from 'jwt-decode';
import { setLoginSuccess } from './store/actions/AuthActions';
import NotFoundComponent from './components/NotFoundComponent';
import setAuthToken from './setAuthToken';
import Footer from './components/Footer/Footer';

// Check for token to keep user logged in
if (localStorage.customerToken) {
  const token = localStorage.customerToken;
  setAuthToken(token);
  const decoded = jwt_decode(token);
  store.dispatch(setLoginSuccess(decoded));
  // const currentTime = Date.now() / 1000; // to get in milliseconds
  // if (decoded.exp < currentTime) {
  //   store.dispatch(logoutUser());
  //   window.location.href = "./login";
  // }
}

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route
            component={Login}
            exact
            path="/login"
          />
          <Route
            component={Register}
            exact
            path="/register"
          />
          <PrivateRoute
            component={Dashboard}
            exact
            path="/"
          />
          <Route component={NotFoundComponent} />
        </Switch>
        <Footer />
      </BrowserRouter>
    </div>
  );
}

export default App;
