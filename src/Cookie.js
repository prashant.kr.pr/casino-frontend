export function createCookie(name, value, days) {
  let expires = ''
  if(days) {
    var date = new Date();
    date.setTime(date.getTime() + days*24*60*60*1000);
    expires = '; expires='+date.toGMTString();
  }
  document.cookie = name+'='+value+expires+';path=/';
}

export function getCookie(cookieName) {
  let match = document.cookie.match(new RegExp(cookieName + '=([^;]+)'));
  if(match) return match[1];
}

export function deleteCookie(cookieName) {
  document.cookie = cookieName + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

export function getCookieValue(name) {
  var cookieVal = document.cookie.match('(^|;)\\s*' + name + '\\s*=\\s*([^;]+)');
  return cookieVal ? cookieVal.pop() : '';
}