import { 
  SET_USERS_LIST_PENDING, 
  SET_USERS_LIST_SUCCESS, 
  SET_USERS_LIST_ERROR, 
} from '../types';

const initialState = {
  isListOfAllUsersStatus: false,
  listOfAllUsers: {},
  listOfAllUsersError: null,

  singleUserStatus: false,
  singleUser: {},
  singleUserError: null,

  updateUserStatus: false,
  updateUser: {},
  updateUserError: null,
}
const userReducer = (state = initialState, action) => {
  switch(action.type) {
    case SET_USERS_LIST_PENDING: 
      return Object.assign({}, state, { 
        isListOfAllUsersStatus: action.status 
      });
    case SET_USERS_LIST_SUCCESS:
      return Object.assign({}, state, { 
        isListOfAllUsersStatus: action.status,
        listOfAllUsers: action.usersList 
      });
    case SET_USERS_LIST_ERROR:
      return Object.assign({}, state, { 
        isListOfAllUsersStatus: action.status,
        listOfAllUsersError: action.usersListError 
      });

    default:
      return state;
  }
}

export default userReducer;