export const subHeaderItems = ['Cricket', 'Football', 'Badminton', 'Chess'];
export const BetListingArr = [
  { 'company': 'CSK vs MI', 'contact': 'Maria Anders', 'pink': '1', 'blue': '2', 'country': 'Germany' },
  { 'company': 'Centro comercial Moctezuma', 'contact': 'Francisco Chang', 'pink': '1', 'blue': '2', 'country': 'Mexico' },
  { 'company': 'Ernst Handel', 'contact': 'Roland Mendel', 'pink': '2', 'blue': '1', 'country': 'Austria' },
  { 'company': 'Island Trading', 'contact': 'Helen Bennett', 'pink': '1', 'blue': '2', 'country': 'UK' },
  { 'company': 'Laughing Bacchus Winecellars', 'contact': 'Yoshi Tannamuri', 'pink': '2', 'blue': '1', 'country': 'Canada' },
  { 'company': 'Magazzini Alimentari Riuniti', 'contact': 'Giovanni Rovelli', 'pink': '1', 'blue': '2', 'country': 'Italy' },
  { 'company': 'Ernst Handel', 'contact': 'Roland Mendel', 'pink': '2', 'blue': '1', 'country': 'Austria' },
]